<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genders extends Model
{
    protected $table = 'Genders';
    protected $fillable = [
    	'GenderId', 
    	'GenderName'
    ];
}
