<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'Images';
    protected $fillable = [
    	'ImageId', 
    	'ImageProductId', 
    	'ImagePath'
    ];

    public function product()
    {
    	return $this->belongsTo('App\Products', 'ImageProductId');
    }
}
