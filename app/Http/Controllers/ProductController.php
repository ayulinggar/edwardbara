<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;
use URL;

use App\User;
use App\Product;
use App\Details;

class ProductController extends Controller
{


    /*
    * Details Product
    */

    public function welcome()
    {
        $newproduct = Product::join('Categories','Categories.CategoryId','ProductCategoryId')
            ->join('Images','Images.ImageProductId','ProductId')
            ->where('ProductIsNew',1)
            ->get();

        $topselling = Product::join('Categories','Categories.CategoryId','ProductCategoryId')
            ->join('Images','Images.ImageProductId','ProductId')
            ->where('ProductSold','>=',1)
            ->get();
            // dd($newproduct);
        return view('welcome')
            ->with('newproduct', $newproduct)
            ->with('topselling', $topselling);
    }

    public function details($productId)
    {
        $productId = base64_decode($productId);
        $product = Product::join('Categories','Categories.CategoryId','ProductCategoryId')
            ->join('Genders','Genders.GenderId','ProductGenderId')
            ->join('Images','Images.ImageProductId','ProductId')
            ->where('ProductId', $productId)
            ->first();
        $color = Details::select('ColorId','ColorName')
            ->join('Products','ProductId','Details.DetailProductId')
            ->join('Colors','Colors.ColorId','Details.DetailColorId')
            ->where('ProductId', $product->ProductId)
            ->distinct('ColorId')
            ->get();
        $size = Details::select('SizeId','SizeName')
            ->join('Products','ProductId','Details.DetailProductId')
            ->join('Size','Size.SizeId','Details.DetailSizeId')
            ->where('ProductId', $product->ProductId)
            ->distinct('SizeId')
            ->get();
        return view('details')
            ->with('product', $product)
            ->with('color', $color)
            ->with('size', $size);
    }


    public function categories()
    {
        return view('categories');
    }

 
    public function shirt()
    {
        $product = Product::join('Categories','Categories.CategoryId','ProductCategoryId')
            ->join('Images','Images.ImageProductId','ProductId')
            ->where('ProductIsNew',1)
            ->get();
            // dd($product);
        return view('catalogue')
            ->with('product', $product);
    }
    
}
