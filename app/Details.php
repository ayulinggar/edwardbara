<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
    protected $table = 'Details';
    protected $fillable = [
    	'DetailId', 
        'DetailProductId', 
        'DetailColorId', 
        'DetailStock', 
        'DetailSizeId'
    ];

    public function product()
    {
    	return $this->belongsTo('App\Products', 'DetailProductId');
    }

    public function color()
    {
    	return $this->belongsTo('App\Colors', 'DetailColorId');
    }
}
