<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'Products';
    protected $fillable = [
    	'ProductId', 
        'ProductGenderId', 
        'ProductCategoryId', 
        'ProductName', 
        'ProductDescriptions', 
        'ProductDiscount', 
        'ProductIsNew', 
        'ProductPrice',
        'ProductSold'
    ];

    public function gender()
    {
    	return $this->belongsTo('App\Genders', 'ProductGenderId');
    }

    public function category()
    {
    	return $this->belongsTo('App\Categories', 'ProductCategoryId');
    }
}
