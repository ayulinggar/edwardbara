<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    protected $table = 'Colors';
    protected $fillable = [
    	'ColorId', 
    	'ColorName'
    ];
}
