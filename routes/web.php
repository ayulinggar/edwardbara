<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@welcome')->name('welcome');
Route::get('/categories', 'ProductController@categories')->name('categories');
Route::get('/blouse', 'ProductController@blouse')->name('blouse');
Route::get('/outer', 'ProductController@outer')->name('outer');
Route::get('/shirt', 'ProductController@shirt')->name('shirt');
Route::get('/pants', 'ProductController@pants')->name('pants');
Route::get('/details/{id}', 'ProductController@details')->name('product-details');
