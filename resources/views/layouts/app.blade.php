<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <base href="http://localhost:8080/bph-migas/public/"> -->
    <base id="baseUrl" href="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/logoutama-icon.png') }}" />
    <title>BPH MIGAS - Badan Pengatur Hilir Minyak dan Gas Bumi</title>

    <!-- Global stylesheets -->
    <!-- <link href="{{ asset('assets/css/font.css') }}" rel="stylesheet" type="text/css"> -->
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/fontawesome/font-awesome-animation.css') }}" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{ asset('assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ui/perfect_scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/layout_fixed_sidebar_custom.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/js/demo_pages/datatables_advanced.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datatables.button/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datatables.button/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datatables.button/buttons.html5.min.js') }}"></script>

    @if (Auth::check())
        <script>
            var TOKEN = @php echo json_encode(Auth::guard("api")->tokenById(Auth::user()->userid()));  @endphp;
            var LANG = "{{ app()->getLocale() }}";
        </script>
    @endif



    <!-- /theme JS files -->
    <style type="text/css">
        .validation-valid-label {
            display: none !important;
        }
    </style>
</head>

<body class="navbar-top" data-url="{{ url('') }}">

    <!-- Main navbar -->
    <div class="navbar navbar-expand-md {{ (Auth::check())?'navbar-light':'navbar-dark' }} fixed-top">

        <!-- Header with logos -->
        <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
            <div class="navbar-brand navbar-brand-md navbar-brand-md-icon-box">
                <a href="{{ url('/') }}" class="d-inline-block">
                    <img src="{{ asset('assets/images/logoutama-light.png') }}" alt="" class="navbar-brand-md-icon">
                </a>
            </div>
            
            <div class="navbar-brand navbar-brand-xs navbar-brand-md-icon-box">
                <a href="{{ url('/') }}" class="d-inline-block">
                    <img src="{{ asset('assets/images/logoutama-icon.png') }}" alt="" class="navbar-brand-md-icon-mini">
                </a>
            </div>
        </div>
        <!-- /header with logos -->

        <!-- Mobile controls -->
        <div class="d-flex flex-1 d-md-none">
            <div class="navbar-brand mr-auto navbar-brand-md-icon-box">
                <!-- <a href="{{ url('/') }}" class="d-inline-block">
                    <img src="{{ asset('assets/images/logoutama-dark.png') }}" alt="" class="navbar-brand-md-icon" style="margin-top:7px;">
                </a> -->
            </div>  

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>

            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>
        <!-- /mobile controls -->


        <!-- Navbar content -->
        <div class="collapse navbar-collapse" id="navbar-mobile">
            @if(Auth::check())
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="javascript:void(0);"  onClick="menu()" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            <span class="navbar-text ml-md-3 mr-md-auto" style="height:1px;"></span>

            
            <ul class="navbar-nav">
                
                <li class="nav-item">
                    <a href="{{ asset('template/Panduan.pdf') }}" class="navbar-nav-link dropdown-toggle caret-0">
                        <i class="icon-book3" style="color:grey;"></i>
                    </a>                        
                </li>
                <li class="nav-item dropdown">

                    @php 
                        $notifications = \NotificationHelper::GetUnReadMessage(); 
                        $numb = count($notifications); 
                    @endphp
                    <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                        <i class="icon-bell3" style="color:grey;"></i>
                        <span class="d-md-none ml-2">Pemberitahuan</span>
                        <!-- <span class="badge badge-mark border-pink-400 ml-auto ml-md-0">1</span> -->
                        <span class="badge bg-danger badge-pill ml-0 ml-md-0">{{ $numb }}</span>
                    </a>
                    
                    <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                        <div class="dropdown-content-header">
                            <span class="font-weight-semibold">Pemberitahuan</span>
                        </div>

                        <div class="dropdown-content-body dropdown-scrollable">
                            <ul class="media-list">
                                @if(isset($notifications))
                                    @foreach($notifications as $notif)
                                        <?php 
                                            if($notif->NotificationTypeId == 1){
                                                $link = '/badanusaha/verifikasibadanusaha/details/'. base64_encode($notif->NotificationFrom.'');
                                            }elseif($notif->NotificationTypeId == 2){
                                                $link = '/badanusaha/pengajuannru';
                                            }else{
                                                $link = '/badanusaha/perpanjangannru'; 
                                            }
                                         ?>
                                        <li class="media">
                                            <div class="media-body">
                                                <div class="media-title">
                                                    <a href="{{ url($link) }}">
                                                        <span class="font-weight-semibold">{{ $notif->NotificationTypeName}}</span>
                                                        <span class="text-muted float-right font-size-sm">{{ date('d/m/Y H:i', strtotime($notif->NoticationDateCreated)) }}</span>
                                                    </a>
                                                </div>
                                                <span class="text-muted">{{ $notif->NotificationContent}}</span>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </li> 
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('assets/images/user.png') }}" class="rounded-circle" alt="">
                        <span>Hallo {{Auth::user()->people->EmployeeNickName}}!</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ asset('template/Panduan.pdf') }}" class="dropdown-item"><i class="icon-download"></i>Download Panduan</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"><i class="icon-switch2"></i>Logout</a>
                     <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                  </form>
                    </div>
                </li>
            </ul>
            @else
            <span class="navbar-text ml-md-3 mr-md-auto" style="height:48px;"></span>
            @endif
        </div>
        <!-- /navbar content -->
        
    </div>
    <!-- /main navbar -->

                    
    <!-- Page content -->
    <div class="page-content">
        
        <!-- Main sidebar -->
        @if(Auth::check())
        <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                BPH MIGAS
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->

            <!-- Sidebar content -->
            <div class="sidebar-content">

                <div class="sidebar-user-material">
                    <div class="sidebar-user-material-body">
                        <div class="card-body text-center">
                            <a href="#">
                                <img src="{{ asset('assets/images/logoutama-icon.png')}}" class="img-fluid rounded-circle shadow-1 mb-3" width="90" height="90" alt="">
                            </a>
                            <h6 class="mb-0 text-white text-shadow-dark">{{Auth::user()->people->EmployeeFullname}}</h6>
                            <i class="icon-user-tie font-size-sm"></i>  {{Auth::user()->access->AccessName}}
                        </div>
                                                    
                        <div class="sidebar-user-material-footer">
                            <a href="#user-na" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>My account</span></a>
                        </div>

                    </div>
                    <div class="collapse" id="user-na">
                        <ul class="nav nav-sidebar">
                            <li class="nav-item">
                                <a href="{{ url('akun/notifikasi') }}" class="nav-link">
                                    <i class="icon-bell3"></i>
                                    <span>Notifikasi</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-stack"></i>
                                    <span>Aktifitas</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('akun/changepassword') }}" class="nav-link">
                                    <i class="icon-lock"></i>
                                    <span>Ganti Password</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" onclick="event.preventDefault();
                                 document.getElementById('logout').submit();"><i class="icon-switch2"></i> <span>Logout</span></a>
                                 <form id="logout" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                     {{ csrf_field() }}
                                  </form>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /user menu -->
                
                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Menu</div> <i class="icon-menu" title="Main"></i></li>
                             <?php echo $menu; ?>
                    </ul>
                </div>
                <!-- /main navigation -->

            </div>
            <!-- /sidebar content -->
            
        </div>
        @endif
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            @yield('content')


            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; 2018. <a href="#">Badan Pengatur Hilir Minyak dan Gas Bumi</a><br>
                    </span>
                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <div id="konfirmasi-simpan" class="modal fade new" tabindex="-1" role="dialog" style="margin-top: 5%;">
        <div class="modal-dialog modal-sm" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-content text-center">
                <div class="modal-header bg-dark">
                    <h4 class="modal-title"><i class="icon-info22"></i> Konfirmasi Penyimpanan Data</h4>
                </div>

                <div class="modal-body with-padding">
                    <p>Apakah anda yakin akan menyimpan data ini?</p>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" id="yakin_konfirm">Simpan</button>
                    <button class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
</html>

<script type="text/javascript">
    function menu(){
        var menu_open = document.getElementById("menu_open");
        if(menu_open.style.display = "block"){
            document.getElementById("menu_open").style.display = "none";
        }else{
            document.getElementById("menu_open").style.display = "block";
        }
    }
    
</script>
