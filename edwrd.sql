-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2019 at 05:58 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edwrd`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CategoryId` int(11) NOT NULL,
  `CategoryName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CategoryId`, `CategoryName`) VALUES
(1, 'Shoes'),
(2, 'Accesories'),
(3, 'Blouse'),
(4, 'Pants'),
(5, 'Outer'),
(6, 'Shirt'),
(7, 'Dress');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `ColorId` int(11) NOT NULL,
  `ColorName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`ColorId`, `ColorName`) VALUES
(1, 'Black'),
(2, 'White'),
(3, 'Red'),
(4, 'Green'),
(5, 'Blue'),
(6, 'Yellow'),
(7, 'Purple'),
(8, 'Grey'),
(9, 'Pink'),
(10, 'Tosca'),
(11, 'Brown'),
(12, 'Orange');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `DetailId` int(11) NOT NULL,
  `DetailProductId` int(11) NOT NULL,
  `DetailColorId` int(11) DEFAULT NULL,
  `DetailStock` int(11) DEFAULT NULL,
  `DetailSizeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`DetailId`, `DetailProductId`, `DetailColorId`, `DetailStock`, `DetailSizeId`) VALUES
(1, 1, 1, 10, 1),
(2, 1, 2, 20, 1),
(3, 1, 1, 15, 2),
(4, 1, 2, 35, 2),
(5, 1, 1, 10, 3),
(6, 1, 2, 20, 3),
(7, 1, 1, 15, 4),
(8, 1, 2, 35, 4),
(9, 2, 8, 10, 1),
(10, 2, 4, 20, 1),
(11, 2, 8, 15, 2),
(12, 2, 4, 35, 2),
(13, 2, 8, 10, 3),
(14, 2, 4, 20, 3),
(15, 2, 8, 15, 4),
(16, 2, 4, 35, 4),
(17, 3, 8, 13, 3),
(18, 3, 8, 26, 4),
(19, 3, 3, 23, 1),
(20, 3, 3, 42, 2),
(21, 4, 5, 23, 1),
(22, 4, 8, 4, 2),
(23, 4, 3, 21, 3),
(24, 4, 9, 2, 4),
(25, 5, 1, 10, 1),
(26, 5, 2, 20, 1),
(27, 5, 1, 15, 2),
(28, 5, 2, 35, 2),
(29, 6, 1, 10, 3),
(30, 6, 2, 20, 3),
(31, 6, 1, 15, 4),
(32, 6, 2, 35, 4),
(33, 6, 8, 10, 1),
(34, 7, 4, 20, 1),
(35, 7, 8, 15, 2),
(36, 7, 4, 35, 2),
(37, 7, 8, 10, 3),
(38, 7, 4, 20, 3),
(39, 8, 8, 15, 4),
(40, 8, 4, 35, 4),
(41, 8, 8, 13, 3),
(42, 9, 8, 26, 4),
(43, 9, 3, 23, 1),
(44, 9, 3, 42, 2),
(45, 10, 5, 23, 1),
(46, 10, 8, 4, 2),
(47, 10, 3, 21, 3),
(48, 10, 9, 2, 4),
(49, 11, 1, 10, 1),
(50, 11, 2, 20, 1),
(51, 12, 1, 15, 2),
(52, 12, 2, 35, 2),
(53, 13, 1, 10, 3),
(54, 13, 2, 20, 3),
(55, 14, 1, 15, 4),
(56, 14, 2, 35, 4),
(57, 14, 8, 10, 1),
(58, 15, 4, 20, 1),
(59, 15, 8, 15, 2),
(60, 16, 4, 35, 2),
(61, 17, 8, 10, 3),
(62, 17, 4, 20, 3),
(63, 18, 8, 15, 4),
(64, 19, 4, 35, 4),
(65, 19, 8, 13, 3),
(66, 20, 8, 26, 4),
(67, 20, 3, 23, 1),
(68, 20, 3, 42, 2),
(69, 21, 5, 23, 1),
(70, 21, 8, 4, 2),
(71, 21, 3, 21, 3),
(72, 22, 9, 2, 4),
(73, 22, 1, 10, 1),
(74, 22, 2, 20, 1),
(75, 23, 1, 15, 2),
(76, 23, 2, 35, 2),
(77, 23, 1, 10, 3),
(78, 24, 2, 20, 3),
(79, 24, 1, 15, 4),
(80, 25, 2, 35, 4),
(81, 25, 8, 10, 1),
(82, 26, 4, 20, 1),
(83, 26, 8, 15, 2),
(84, 27, 4, 35, 2),
(85, 28, 8, 10, 3),
(86, 29, 4, 20, 3),
(87, 30, 8, 15, 4),
(88, 31, 4, 35, 4),
(89, 31, 8, 13, 3),
(90, 32, 8, 26, 4),
(91, 32, 3, 23, 1),
(92, 33, 3, 42, 2),
(93, 33, 5, 23, 1),
(94, 34, 8, 4, 2),
(95, 34, 3, 21, 3),
(96, 35, 9, 2, 4),
(97, 35, 1, 10, 1),
(98, 36, 2, 20, 1),
(99, 37, 1, 15, 2),
(100, 38, 2, 35, 2),
(101, 38, 1, 10, 3),
(102, 39, 2, 20, 3),
(103, 40, 1, 15, 4),
(104, 41, 2, 35, 4),
(105, 41, 8, 10, 1),
(106, 42, 4, 20, 1),
(107, 42, 8, 15, 2),
(108, 42, 4, 35, 2),
(109, 43, 8, 10, 3),
(110, 43, 4, 20, 3),
(111, 44, 8, 15, 4),
(112, 45, 4, 35, 4),
(113, 45, 8, 13, 3),
(114, 46, 8, 26, 4),
(115, 46, 3, 23, 1),
(116, 46, 3, 42, 2),
(117, 47, 5, 23, 1),
(118, 47, 8, 4, 2),
(119, 48, 1, 10, 1),
(120, 49, 2, 20, 1),
(121, 50, 1, 15, 2),
(122, 51, 4, 35, 2),
(123, 51, 8, 10, 3),
(124, 51, 4, 20, 3),
(125, 52, 8, 15, 4),
(126, 52, 4, 35, 4),
(127, 53, 8, 13, 3),
(128, 53, 8, 26, 4),
(129, 53, 3, 23, 1),
(130, 53, 3, 42, 2),
(131, 54, 5, 23, 1),
(132, 55, 8, 4, 2),
(133, 56, 1, 10, 1),
(134, 56, 2, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `GenderId` int(11) NOT NULL,
  `GenderName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`GenderId`, `GenderName`) VALUES
(1, 'Man'),
(2, 'Women');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `ImageId` int(11) NOT NULL,
  `ImageProductId` int(11) DEFAULT NULL,
  `ImagePath` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`ImageId`, `ImageProductId`, `ImagePath`) VALUES
(1, 1, 'b-men1.jpg'),
(2, 2, 'b-men2.jpg'),
(3, 3, 'b-men3.jpg'),
(4, 4, 'b-men4.jpg'),
(5, 5, 'b-men5.jpg'),
(6, 6, 'b-men6.jpg'),
(7, 7, 'b-men7.jpg'),
(8, 8, 'b-men8.jpg'),
(9, 9, 'men1.jpg'),
(10, 10, 'men2.jpg'),
(11, 11, 'men3.jpg'),
(12, 12, 'men4.jpg'),
(13, 13, 'men5.jpg'),
(14, 14, 'men6.jpg'),
(15, 15, 'men7.jpg'),
(16, 16, 'men8.jpg'),
(17, 17, 'p-men1.jpg'),
(18, 18, 'p-women2.jpg'),
(19, 19, 'p-men3.jpg'),
(20, 20, 'p-men4.jpg'),
(21, 21, 'p-men5.jpg'),
(22, 22, 'p-men6.jpg'),
(23, 23, 'p-men7.jpg'),
(24, 24, 'p-men8.jpg'),
(25, 25, 'b-women1.jpg'),
(26, 26, 'b-women2.jpg'),
(27, 27, 'b-women3.jpg'),
(28, 28, 'b-women4.jpg'),
(29, 29, 'b-women5.jpg'),
(30, 30, 'b-women6.jpg'),
(31, 31, 'b-women7.jpg'),
(32, 32, 'b-women8.jpg'),
(33, 33, 'women1.jpg'),
(34, 34, 'women2.jpg'),
(35, 35, 'women3.jpg'),
(36, 36, 'women4.jpg'),
(37, 37, 'women5.jpg'),
(38, 38, 'women6.jpg'),
(39, 39, 'women7.jpg'),
(40, 40, 'women8.jpg'),
(41, 41, 'p-women1.jpg'),
(42, 42, 'p-women2.jpg'),
(43, 43, 'p-women3.jpg'),
(44, 44, 'p-women4.jpg'),
(45, 45, 'p-women5.jpg'),
(46, 46, 'p-women6.jpg'),
(47, 47, 'p-women7.jpg'),
(48, 48, 'p-women8.jpg'),
(49, 49, 'o-women1.jpg'),
(50, 50, 'o-women2.jpg'),
(51, 51, 'o-women3.jpg'),
(52, 52, 'o-women4.jpg'),
(53, 53, 'o-women5.jpg'),
(54, 54, 'o-women6.jpg'),
(55, 55, 'o-women7.jpg'),
(56, 56, 'o-women8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductId` int(11) NOT NULL,
  `ProductGenderId` int(11) DEFAULT NULL,
  `ProductCategoryId` int(11) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `ProductDescriptions` varchar(255) DEFAULT NULL,
  `ProductDiscount` int(11) DEFAULT NULL,
  `ProductIsNew` int(11) DEFAULT NULL,
  `ProductPrice` double DEFAULT NULL,
  `ProductSold` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductId`, `ProductGenderId`, `ProductCategoryId`, `ProductName`, `ProductDescriptions`, `ProductDiscount`, `ProductIsNew`, `ProductPrice`, `ProductSold`) VALUES
(1, 1, 3, 'Men Blouse 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 325000, NULL),
(2, 1, 3, 'Men Blouse 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 400000, NULL),
(3, 1, 3, 'Men Blouse 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 320000, NULL),
(4, 1, 3, 'Men Blouse 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 500000, NULL),
(5, 1, 3, 'Men Blouse 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 450000, NULL),
(6, 1, 3, 'Men Blouse 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 276000, NULL),
(7, 1, 3, 'Men Blouse 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 335000, NULL),
(8, 1, 3, 'Men Blouse 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 295000, NULL),
(9, 1, 6, 'Men Shirt 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 396000, NULL),
(10, 1, 6, 'Men Shirt 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 340000, NULL),
(11, 1, 6, 'Men Shirt 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 200000, NULL),
(12, 1, 6, 'Men Shirt 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 257000, NULL),
(13, 1, 6, 'Men Shirt 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 325000, NULL),
(14, 1, 6, 'Men Shirt 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 325000, NULL),
(15, 1, 6, 'Men Shirt 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 400000, NULL),
(16, 1, 6, 'Men Shirt 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 320000, NULL),
(17, 1, 4, 'Men Pants 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 500000, NULL),
(18, 1, 4, 'Men Pants 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 450000, NULL),
(19, 1, 4, 'Men Pants 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 276000, NULL),
(20, 1, 4, 'Men Pants 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 335000, NULL),
(21, 1, 4, 'Men Pants 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 295000, NULL),
(22, 1, 4, 'Men Pants 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 396000, NULL),
(23, 1, 4, 'Men Pants 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 325000, NULL),
(24, 1, 4, 'Men Pants 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 400000, NULL),
(25, 2, 6, 'Women Shirt 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 320000, NULL),
(26, 2, 6, 'Women Shirt 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 500000, NULL),
(27, 2, 6, 'Women Shirt 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 450000, NULL),
(28, 2, 6, 'Women Shirt 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 276000, NULL),
(29, 2, 6, 'Women Shirt 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 335000, NULL),
(30, 2, 6, 'Women Shirt 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 295000, NULL),
(31, 2, 6, 'Women Shirt 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 396000, NULL),
(32, 2, 6, 'Women Shirt 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, 1, 340000, NULL),
(33, 2, 3, 'Women Blouse 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 30, 1, 200000, NULL),
(34, 2, 3, 'Women Blouse 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 257000, 2),
(35, 2, 3, 'Women Blouse 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 325000, NULL),
(36, 2, 3, 'Women Blouse 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 400000, NULL),
(37, 2, 3, 'Women Blouse 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 20, 1, 320000, 46),
(38, 2, 3, 'Women Blouse 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 500000, NULL),
(39, 2, 3, 'Women Blouse 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 450000, NULL),
(40, 2, 3, 'Women Blouse 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 276000, 5),
(41, 2, 4, 'Women Pants 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 335000, NULL),
(42, 2, 4, 'Women Pants 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 10, 1, 295000, NULL),
(43, 2, 4, 'Women Pants 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 396000, 8),
(44, 2, 4, 'Women Pants 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 340000, NULL),
(45, 2, 4, 'Women Pants 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 200000, NULL),
(46, 2, 4, 'Women Pants 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 257000, NULL),
(47, 2, 4, 'Women Pants 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 5, 1, 325000, NULL),
(48, 2, 4, 'Women Pants 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 400000, 5),
(49, 2, 5, 'Women Outer 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 320000, NULL),
(50, 2, 5, 'Women Outer 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 25, 1, 500000, 6),
(51, 2, 5, 'Women Outer 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 450000, NULL),
(52, 2, 5, 'Women Outer 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, 1, 276000, NULL),
(53, 2, 5, 'Women Outer 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 335000, NULL),
(54, 2, 5, 'Women Outer 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 295000, NULL),
(55, 2, 5, 'Women Outer 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, 1, 396000, NULL),
(56, 2, 5, 'Women Outer 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL, NULL, 340000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE `size` (
  `SizeId` int(11) NOT NULL,
  `SizeName` varchar(5) DEFAULT NULL,
  `SizeDescription` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`SizeId`, `SizeName`, `SizeDescription`) VALUES
(1, 'S', 'Small'),
(2, 'M', 'Medium'),
(3, 'L', 'Large'),
(4, 'XL', 'Extra Large');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`ColorId`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`DetailId`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`GenderId`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`ImageId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductId`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`SizeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `ColorId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `DetailId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `GenderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `ImageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `size`
--
ALTER TABLE `size`
  MODIFY `SizeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
